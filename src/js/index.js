const burger = document.getElementById('burger-button');
const burgerMenu = document.querySelector('.burger-menu');
const firstLine = document.querySelector('.burger__line--first');
const middleLine = document.querySelector('.burger__line--middle');
const lastLine = document.querySelector('.burger__line--last');

// console.log('Hello World');

burger.addEventListener('click', showBurgerMenu);

function showBurgerMenu(){
    // debugger
    burgerMenu.classList.toggle('active');
    firstLine.classList.toggle('active');
    if (burgerMenu.classList.contains('active')){
        middleLine.style.transform = 'rotate(' + 45 + 'deg) translateY(3px) translateX(3px)';
        lastLine.style.transform = 'rotate(' + 135 + 'deg) translateY(3px) translateX(-3px)';
        firstLine.style.transform = 'translateX(-50px)';
    }else{
        firstLine.style.transform = 'rotate(' + 0 + 'deg)';
        lastLine.style.transform = 'rotate(' + 0 + 'deg)';
        middleLine.style.transform = 'translateX(0px)';
    }
}

